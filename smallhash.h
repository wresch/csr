/******************************************************************************/
/* This is an implementation of a small hash table with string                */
/* keys. It is meant to stay below a utilization of 50% since                 */
/* it uses linear probing and. It is also fixed size and once                 */
/* a key can't be insert it will fail. It's mean for                          */
/* small data sets like mapping chromosome names to their sizes.              */
/******************************************************************************/

#ifndef SMALLHASH_H
#define SMALLHASH_H

#include <inttypes.h>
#include <stdbool.h>

struct sh_kv {
    char *key;
    size_t hash;  //fewer strcmp, more storage
    uint32_t val;
};

struct sh_table {
    struct sh_kv *table;
    size_t size;
};

extern bool sh_table_new(size_t size, struct sh_table **tab);
extern void sh_table_free(struct sh_table **table);
extern bool sh_table_insert(struct sh_table *table, const char *key, const uint32_t val);
extern bool sh_table_get(struct sh_table *table, const char *key, uint32_t *val);

#endif
