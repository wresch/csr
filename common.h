#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
/* guesses if a file is a sequence file from the extension */
bool is_seq(const char *str); 
/* guesses if a file is a fastq file from the extension */
bool is_fastq(const char *str); 
/* guesses if a file is an alignment file from the extension 
 * (sam and bam only) */
bool is_aln(const char *str);

#endif
