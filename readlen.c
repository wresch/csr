#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>
#include <zlib.h>  

#include "debug.h"
#include "common.h"
#include "argtable3.h"

#include "htslib/kseq.h"  
#include "htslib/hts.h"
#include "htslib/sam.h"

static void readlen_usage(FILE *fp, void **argtable) {
    fprintf(fp,
            "\nNAME\n"
            "    csr readlen - determines read length from sequence and alignment files\n"
            "SYNOPSIS\n"
            "    csr readlen ");
    arg_print_syntax(fp, argtable, "\n");
    fprintf(fp, "OPTIONS/ARGUMENTS\n");
    arg_print_glossary(fp, argtable, "    %-25s %s\n");
    fprintf(fp,
            "DESCRIPTION\n"
            "    Takes a sequence file (fasta/fastq, compressed or not)\n"
            "    or a alignment file (sam or bam) and determines read length.\n"
            "    The output format is\n\n"
            "        mean:min-max:number_of_sequences\n\n");
}

KSEQ_INIT(gzFile, gzread)
  
int main_readlen(int argc, char *argv[])  {  
    int rc = EXIT_SUCCESS;
    
    /* set up command line interface */
    struct arg_lit *help;
    struct arg_file *infile;
    struct arg_end *end;
    int cli_nerrors = 0;

    void *argtable[] = {
        help = arg_litn("h", "help", 0, 1, "show this help message"),
        infile = arg_filen(NULL, NULL, "<infile>", 1, 1, "input file (sam/bam or fastq/fasta)"),
        end = arg_end(20),
    };

    cli_nerrors = arg_parse(argc,argv,argtable);

    if (help->count > 0) {
        readlen_usage(stdout, argtable);
        goto cleanup;
    }
    if (cli_nerrors > 0) {
        arg_print_errors(stderr, end, "csr readlen");
        fprintf(stderr, "Use 'csr readlen --help' for more information.\n");
        rc = EXIT_FAILURE;
        goto cleanup;
    }



    // variables for counting read lengths
    size_t n = 0;
    size_t minl = 100000;
    size_t maxl = 0;
    size_t suml = 0;
    size_t l = 0;
    if (is_seq(infile->filename[0])) {
        gzFile fp = NULL;  
        kseq_t *seq = NULL;  

        fp = gzopen(infile->filename[0], "r");
        if (!fp) {rc = EXIT_FAILURE; goto cleanup_seq;}

        seq = kseq_init(fp);
        while ((n < 50000) && ((kseq_read(seq)) >= 0)) {
            if (seq->seq.l < minl) {
                minl = seq->seq.l;
            } else if (seq->seq.l > maxl) {
                maxl = seq->seq.l;
            }
            suml += seq->seq.l;
            n++;
        }
cleanup_seq:
        if (seq) kseq_destroy(seq);
        if (fp) gzclose(fp);
        if (rc != EXIT_SUCCESS) goto cleanup;
    } else if (is_aln(infile->filename[0])) {
        samFile *inf = NULL;
        bam_hdr_t *header = NULL;
        char *fmt = NULL;
        bam1_t *aln = NULL;

        inf = sam_open(infile->filename[0], "r");
        if (!inf) {rc=EXIT_FAILURE; goto cleanup_aln;}
        header = sam_hdr_read(inf);
        if (!header) {rc=EXIT_FAILURE; goto cleanup_aln;}

        fmt = hts_format_description(hts_get_format(inf));
        if (!fmt) {rc=EXIT_FAILURE; goto cleanup_aln;}
        log_info("input file is in %s format", fmt);

        aln = bam_init1();
        while ((n < 50000) && (sam_read1(inf, header, aln) >= 0)) {
            if (aln->core.l_qseq >= 0) {
                l = (size_t)aln->core.l_qseq;
            } else { 
                rc = EXIT_FAILURE;
                goto cleanup_aln;
            }
            if (l < minl) {
                minl = l;
            } else if (l > maxl) {
                maxl = l;
            }
            suml += l;
            n++;
        }
cleanup_aln:
        if (inf) sam_close(inf);
        if (fmt) free(fmt);
        if (aln) bam_destroy1(aln);
        if (header) bam_hdr_destroy(header);
        if (rc != EXIT_SUCCESS) goto cleanup;
    } else {
        log_error("%s does not seem to be a fasta/fastq/sam/bam file", argv[1]);
        return EXIT_FAILURE;
    }
    printf("%d:%zu-%zu:%zu\n", (int)round((double)suml / n), minl, maxl, n);
    return EXIT_SUCCESS;

cleanup:
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return rc;
}
