#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#ifdef NDEBUG
#define DEVEL 0
#else
#define DEVEL 1
#endif

#define log_debug(M, ...) \
	do {if ( DEVEL ) fprintf(stderr, "DEBUG(%s|%d|%s): " M "\n", \
			       __FILE__ , __LINE__ , __func__ , ##__VA_ARGS__ );} while(0)

#define log_error(M, ...)	\
	do {fprintf(stderr, "ERROR: " M "\n" , ##__VA_ARGS__); } while(0)

#define log_info(M, ...)	\
	do {fprintf(stderr, "INFO:  " M "\n" , ##__VA_ARGS__); } while(0)



#ifdef TEST
#define check_ok(x, M) do {if (x) {\
        fprintf(stderr, "PASS - %s\n", M);\
    } else {\
        fprintf(stderr, "FAIL - %s\n", M);}} while (0)
#else
#define check_ok(x, M) do {} while(0)
#endif


#endif
