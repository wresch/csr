#include <zlib.h>  
#include <stdio.h>  
#include <stdint.h>
#include "htslib/kseq.h"  
#include "debug.h"
#include "common.h"

/** Todo: better error checking */

KSEQ_INIT(gzFile, gzread)

static void qual_scale_usage(FILE *fp) {
    fprintf(fp,
            "\nNAME\n"
            "    csr fastq-qual-scale - determines quality scale of fastq file\n"
            "SYNOPSIS\n"
            "    csr fastq-qual-scale <infile>\n"
            "ARGUMENTS\n"
            "    <infile>  fastq file\n"
            "OPTIONS\n"
            "    -h, --help Show this help message\n"
            "DESCRIPTION\n"
            "    Returns a symbolic description of the quality scale and\n"
            "    the observed numeric range based on the first 50000 reads\n"
            "    in the format\n\n"
            "        name:min-max:number_of_sequences\n\n"
            "    Input file can be fastq or gzip compressed fastq\n");
}

int main_qual_scale(int argc, char *argv[])  {  
    gzFile fp = NULL;  
    kseq_t *seq = NULL;  
    int l = -1;  
    if (argc < 2) {  
        qual_scale_usage(stderr);
    }  
    if (strcmp(argv[1], "help") == 0 
            || strcmp(argv[1], "--help") == 0 
            || strcmp(argv[1], "-h") == 0) {
        qual_scale_usage(stdout);
        return EXIT_SUCCESS;
    }
    if (!is_fastq(argv[1])) {
        log_error("'%s' does not appear to be a fastq file", argv[1]);
        return EXIT_FAILURE;
    }

    fp = gzopen(argv[1], "r");
    seq = kseq_init(fp);
    unsigned int n = 0;
    int minq = 255;
    int maxq = 0;
    int q = 0;
    while ((n < 50000) && (l = kseq_read(seq)) >= 0) {
        for (char *a = seq->qual.s; *a != '\0'; a++) {
            q = (int)(*a);
            if (q > maxq) {
                maxq = q;
            } else if (q < minq) {
                minq = q;
            }
        }
        n++;
    }  
    kseq_destroy(seq);
    gzclose(fp);
    if (minq < 59) {
        printf("phred33:%d-%d:%d\n", minq, maxq, n);
    } else {
        if (minq == 59) {
            printf("solexa64:%d-%d:%d\n", minq, maxq, n);
        } else {
            printf("phred64:%d-%d:%d\n", minq, maxq, n);
        }
    }
    return EXIT_SUCCESS;  
} 
