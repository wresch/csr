#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

#include "debug.h"
#include "common.h"
#include "argtable3.h"
#include "htslib/sam.h"


static void binbam_usage(FILE *fp, void **argtable) {
    fprintf(fp,
            "\nNAME\n"
            "    csr binbam - creates a density track of reads in bins from bam file\n"
            "SYNOPSIS\n"
            "    csr binbam ");
    arg_print_syntax(fp, argtable, "\n");
    fprintf(fp, "OPTIONS/ARGUMENTS\n");
    arg_print_glossary(fp, argtable, "    %-25s %s\n");
    fprintf(fp,
            "DESCRIPTION\n"
            "    Calculate density of reads in bins across the genome from bam file.\n"
            "    Output units:  RPKM\n"
            "    Output format: Bedgraph\n"
            "NOTES\n"
            "    * Bam input file has to be sorted, but an index is not required\n"
            "    * Output goes to stdout\n"
            "    * Currently ignores chrM and gapped or local alignemts (where the\n"
            "      aligned length is not the same as the read length).\n"
            );
}

/******************************************************************************/
/* The function that does the binning. it calls the output function to create */
/* the bedgraph format output. The last (partial) bin is discarded            */
/******************************************************************************/
static bool binbam(const char *inf_name, int binsize, int frag_size,
        int redundancy, int sg, bool bystrand) {

    bool ok = true;
    samFile *inf = NULL;
    bam_hdr_t *header = NULL;
    bam1_t *aln = NULL;

    inf = sam_open(inf_name, "r");
    if (!inf) {
        ok = false; 
        goto cleanup;
    }
    header = sam_hdr_read(inf);
    if (!header) {
        ok = false; 
        goto cleanup;
    }
    log_info("Reference contains %"PRIi32" sequences", header->n_targets);
    int32_t curr_tid = -1;
    int32_t n_refs_found = 0;
    aln = bam_init1();

    bam1_core_t core;
    while (sam_read1(inf, header, aln) >= 0) {
        core = aln->core;
        if (core.tid != curr_tid) {
            // output data for previous chrom
            curr_tid = core.tid;
            n_refs_found++;
            if (n_refs_found > header->n_targets) {
                log_error("bam file %s appears to be unsorted", inf_name);
                ok = false;
                goto cleanup;
            }
        }
    }
    log_info("Found %"PRIi32" reference sequences in the alignments", n_refs_found);
        

cleanup:
    if (inf) sam_close(inf);
    if (header) bam_hdr_destroy(header);
    if (aln) bam_destroy1(aln);
    return ok;
}

int main_binbam(int argc, char *argv[])  {  
    int rc = EXIT_SUCCESS;
    int cli_nerrors = 0;

    /* parse command line arguments */
    struct arg_int *frag_size, *redundancy, *sg, *binsize;
    struct arg_lit *by_strand, *help;
    struct arg_str *trackline;
    struct arg_file *infile;
    struct arg_end *end;

    void *argtable[] = {
        help = arg_litn("h", "help", 0, 1, "show this help message"),
        frag_size = arg_intn("f", "frag-size", "<n>", 0, 1, 
                "Size of fragments; reads are shifted by half fragsize [0]"),
        redundancy = arg_intn("n", "redundancy", "<n>", 0, 1,
                "Number of identical alignments per position allowed [3]"),
        sg = arg_intn(NULL, "sg", "<n>", 0, 1, 
                "width (in terms of bins) of optional Savitzky Golay filter [0]"),
        by_strand = arg_litn("s", "by-strand", 0, 1, 
                "Output separate densities by strand [false]"),
        trackline = arg_strn("t", "track-line", "<str>", 0, 1,
                "Add this line before the output [none]"),
        infile = arg_filen(NULL, NULL, "<infile>", 1, 1,
                "Bam/Sam input file"),
        binsize = arg_intn(NULL, NULL, "<binsize>", 1, 1,
                "Size of bins to use"),
        end = arg_end(20),
    };
    frag_size->ival[0] = 0;
    redundancy->ival[0] = 3;
    sg->ival[0] = 0;
    trackline->sval[0] = NULL;

    cli_nerrors = arg_parse(argc,argv,argtable);
    if (help->count > 0) {
        binbam_usage(stdout, argtable);
        goto cleanup;
    }
    if (cli_nerrors > 0) {
        arg_print_errors(stderr, end, "csr binbam");
        fprintf(stderr, "Use 'csr binbam --help' for more information.\n");
        rc = EXIT_FAILURE;
        goto cleanup;
    }

    /* Todo: if requested, print out trackline */
    binbam(infile->filename[0], binsize->ival[0], frag_size->ival[0],
            redundancy->ival[0], sg->ival[0], by_strand->count > 0);


        
    
cleanup:
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return rc;
}
