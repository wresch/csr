#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "common.h"
#include "argtable3.h"


static void TOOL_usage(FILE *fp, void **argtable) {
    fprintf(fp,
            "\nNAME\n"
            "    csr TOOL - \n"
            "SYNOPSIS\n"
            "    csr TOOL ");
    arg_print_syntax(fp, argtable, "\n");
    fprintf(fp, "OPTIONS/ARGUMENTS\n");
    arg_print_glossary(fp, argtable, "    %-25s %s\n");
    fprintf(fp,
            "DESCRIPTION\n"
            "    \n"
            "    \n"
            "    \n");
}

int main_TOOL(int argc, char *argv[])  {  
    int rc = EXIT_SUCCESS;
    
    /* set up command line interface */
    struct arg_lit *help;
    struct arg_end *end;
    int cli_nerrors = 0;

    void *argtable[] = {
        help = arg_litn("h", "help", 0, 1, "show this help message"),
        end = arg_end(20),
    };

    cli_nerrors = arg_parse(argc,argv,argtable);

    if (help->count > 0) {
        TOOL_usage(stderr, argtable);
        goto cleanup;
    }
    if (cli_nerrors > 0) {
        arg_print_errors(stderr, end, "csr TOOL");
        fprintf(stderr, "Use 'csr TOOL --help' for more information.\n");
        rc = EXIT_FAILURE;
        goto cleanup;
    }


cleanup:
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    return rc;
}
