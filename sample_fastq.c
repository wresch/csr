#include <zlib.h>  
#include <stdio.h>  
#include <stdlib.h>
#include <time.h>
#include "kseq.h"  

KSEQ_INIT(gzFile, gzread)  
  
int main(int argc, char *argv[])  
{  
    gzFile fp;  
    kseq_t *seq;  
    int l;  
    if (argc != 3) {  
        fprintf(stderr, "Usage: %s <in.seq> frac_sample\n", argv[0]);  
        return 1;  
    }  
    double frac = atof(argv[2]);
    if (frac == 0.0) {
        fprintf(stderr, "%s is not a valid fraction for sub-sampling\n", argv[2]);
        return 1;
    }
    // use integral part of frac_sample as random seed
    unsigned int seed = (unsigned int)frac;
    if (seed == 0) {
        seed = (unsigned int)time(NULL);
    } else {
        frac -= (double)seed;
    }

    //fprintf(stdout, "random seed: %d\n", seed);
    //fprintf(stdout, "fraction:    %f\n", frac);
    srand(seed);

    fp = gzopen(argv[1], "r"); // STEP 2: open the file handler  
    seq = kseq_init(fp); // STEP 3: initialize seq  
    unsigned int n = 0;
    while ((l = kseq_read(seq)) >= 0) { // STEP 4: read sequence  
        if ((float)rand() / RAND_MAX <= frac) {
            n++;
            printf("@%s %s\n%s\n+\n%s\n", seq->name.s, seq->comment.s,
                    seq->seq.s, seq->qual.s);
        }
    }  
    kseq_destroy(seq); // STEP 5: destroy seq  
    gzclose(fp); // STEP 6: close the file handler  
    fprintf(stderr, "%d\n", n);
    return 0;  
} 
