#include <zlib.h>  
#include <stdio.h>  
#include <inttypes.h>
#include "htslib/kseq.h"  
#include "debug.h"
#include "common.h"

/** Todo: better error checking */

KSEQ_INIT(gzFile, gzread)

static void fqinfo_usage(FILE *fp) {
    fprintf(fp,
            "\nNAME\n"
            "    csr fqinfo - basic stats about fastq file\n"
            "SYNOPSIS\n"
            "    csr fqinfo <infile>\n"
            "ARGUMENTS\n"
            "    <infile>  fastq file\n"
            "OPTIONS\n"
            "    -h, --help Show this help message\n"
            "DESCRIPTION\n"
            "    Returns basic info about fastq file (number of reads,\n"
            "    quality scale, ...)\n");
}

int main_fqinfo(int argc, char *argv[])  {  
    gzFile fp = NULL;  
    kseq_t *seq = NULL;  
    int l = -1;  
    if (argc < 2) {  
        fqinfo_usage(stderr);
    }  
    if (strcmp(argv[1], "help") == 0 
            || strcmp(argv[1], "--help") == 0 
            || strcmp(argv[1], "-h") == 0) {
        fqinfo_usage(stdout);
        return EXIT_SUCCESS;
    }
    if (!is_fastq(argv[1])) {
        log_error("'%s' does not appear to be a fastq file", argv[1]);
        return EXIT_FAILURE;
    }

    fp = gzopen(argv[1], "r");
    seq = kseq_init(fp);
    int minq = 255;
    int maxq = 0;
    int q = 0;
    uint64_t nbases = 0;
    uint64_t nreads = 0;
    uint64_t minlen = 1000000;
    uint64_t maxlen = 0;
    while ((l = kseq_read(seq)) >= 0) {
        uint64_t ul = (uint64_t)l;
        for (char *a = seq->qual.s; *a != '\0'; a++) {
            q = (int)(*a);
            if (q > maxq) {
                maxq = q;
            } else if (q < minq) {
                minq = q;
            }
        }
        nreads++;
        nbases += ul;
        if (ul > maxlen) {
            maxlen = l;
        }
        if (ul < minlen) {
            minlen = l;
        }
    }  
    kseq_destroy(seq);
    gzclose(fp);
    if (minq < 59) {
        printf("QUALSCALE  phred33:%d-%d:%"PRIu64"\n", minq, maxq, nreads);
    } else {
        if (minq == 59) {
            printf("QUALSCALE  solexa64:%d-%d:%"PRIu64"\n", minq, maxq, nreads);
        } else {
            printf("QUALSCALE  phred64:%d-%d:%"PRIu64"\n", minq, maxq, nreads);
        }
    }
    printf("NREADS     %"PRIu64"\n", nreads);
    printf("NBASES     %"PRIu64"\n", nbases);
    return EXIT_SUCCESS;  
} 
