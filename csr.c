#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "debug.h"
#include "htslib/hts.h"


int main_qual_scale(int argc, char *argv[]);
int main_fqinfo(int argc, char *argv[]);
int main_readlen(int argc, char *argv[]);
int main_binbam(int argc, char *argv[]);


struct cmd {
    int (*func)(int, char*[]);
    const char *name;
    const char *help;
};

static struct cmd cmds[] = {
    { 
        .func = main_qual_scale,
        .name = "qual-scale",
        .help = "determines quality scale of fastq file"
    },
    {
        .func = main_fqinfo,
        .name = "fqinfo",
        .help = "basic stats for fastq file"
    },
    {
        .func = main_readlen,
        .name = "readlen",
        .help = "determines read length from sequence and alignment file"
    },
    {
        .func = main_binbam,
        .name = "binbam",
        .help = "creates a density track of reads in bins from bam file"
    },
    {
        .func = NULL,
        .name = NULL,
        .help = NULL
    }
};


static void usage(FILE *fp) {
    fprintf(fp,
            "\n"
            "NAME\n"
            "    csr - htslib based tools for fastq/bam files\n"
            "SYNOPSIS\n"
            "    csr command [options]\n"
            "COMMANDS\n");
    size_t i = 0;
    while (cmds[i].name) {
        fprintf(fp, "    %16s - %s\n", cmds[i].name, cmds[i].help);
        i++;
    }
    fprintf(fp, "\n");
}


int main(int argc, char *argv[]) {
    if (argc < 2) { usage(stderr); return 1; }
    if (strcmp(argv[1], "help") == 0 
            || strcmp(argv[1], "--help") == 0 
            || strcmp(argv[1], "-h") == 0) {
        usage(stdout);
        return EXIT_SUCCESS;
    } else {
        size_t i = 0;
        while (cmds[i].name) {
            if (strcmp(argv[1], cmds[i].name) == 0) {
                return cmds[i].func(argc - 1, argv + 1);
            }
            i++;
        }
    }
    log_error("unrecognized command: %s", argv[1]);
    return EXIT_FAILURE;
}
