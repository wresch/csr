#include "common.h"

#include <string.h>

static bool endswith(const char *str, const char *suffix) {
    if (!str || !suffix) {return false;}
    size_t lenstr = strlen(str);
    size_t lensuffix = strlen(suffix);
    if (lensuffix >  lenstr) {return false;}
    return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

static bool endswith_set(const char *str, const char *suffixes[]) {
    size_t i = 0;
    while(true) {
        if (suffixes[i] == NULL) {break;}
        if (endswith(str, suffixes[i])) {
            return true;
        }
        i++;
    }
    return false;
}

bool is_seq(const char *str) {
    static const char *suffixes[] = {
        ".fasta", ".fasta.gz", ".fa", ".fa.gz", 
        ".fastq", ".fastq.gz", ".fq", ".fq.gz",
        NULL
    };
    return endswith_set(str, suffixes);
}

bool is_fastq(const char *str) {
    static const char *suffixes[] = {
        ".fastq", ".fastq.gz", ".fq", ".fq.gz", 
        NULL
    };
    return endswith_set(str, suffixes);
}

bool is_aln(const char *str) {
    static const char *suffixes[] = {
        ".sam", ".bam", NULL
    };
    return endswith_set(str, suffixes);
}
