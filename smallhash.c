#include <string.h>
#include <stdlib.h>

#include "smallhash.h"
#include "debug.h"

/* djb2
 * This algorithm was first reported by Dan Bernstein
 * many years ago in comp.lang.c. It's not the best but will do for this
 * simple hash table
 */
static size_t hash(const char *str)
{
    size_t h = 5381;
    unsigned int c; 
    while ((c = (unsigned int)*str++) != '\0') {
        h = ((h << 5) + h) ^ c; // hash*33 + c
    }
    return h;
}

extern bool sh_table_new(size_t size, struct sh_table **table) {
    *table = calloc(1, sizeof(struct sh_table));
    if (*table == NULL) {
        return false;
    }
    (*table)->table = calloc(size, sizeof(struct sh_kv));
    if ((*table)->table == NULL) {
        free(*table);
        *table = NULL;
        return false;
    }
    (*table)->size = size;
    return true;
}

extern void sh_table_free(struct sh_table **table) {
    if (!(*table)) {
        return;
    }
    if ((*table)->table) {
        for (size_t i = 0; i < (*table)->size; i++) {
            free((*table)->table[i].key);
        }
        free((*table)->table);
    }
    free((*table));
    *table = NULL;
}

    
extern bool sh_table_insert(struct sh_table *table, const char *key, const uint32_t val) {
    if (table == NULL) {
        return false;
    }
    size_t h = hash(key) % table->size;
    size_t i = h;
    size_t ntries = 0;
    while (table->table[i].key != NULL) {
        if (table->table[i].hash == h) {
            return false;
        }
        i++;
        if (i == table->size) {
            i = 0;
        }
        ntries++;
        if (ntries == table->size) {
            return false;
        }
    }
    table->table[i].hash = h;
    table->table[i].val = val;
    table->table[i].key = calloc((strlen(key) + 1), sizeof(char));
    if (table->table[i].key == NULL) {
        return false;
    }
    strcpy(table->table[i].key, key);
    return true;
}

extern bool sh_table_get(struct sh_table *table, const char *key, uint32_t *val) {
    if (table == NULL) {
        *val = 0;
        return false;
    }
    size_t h = hash(key) % table->size;
    size_t i = h;
    size_t ntries = 0;
    while (table->table[i].key != NULL) {
        if (table->table[i].hash == h) {
            *val = table->table[i].val;
            return true;
        }
        i++;
        if (i == table->size) {
            i = 0;
        }
        ntries++;
        if (ntries == table->size) {
            *val = 0;
            return false;
        }
    }
    *val = 0;
    return false;
}


#ifdef TEST
int main(void) {
    bool ok;
    struct sh_table *table = NULL;
    ok = sh_table_new(256, &table);
    if (ok) {
        // insert some keys
        ok = sh_table_insert(table, "norm", 1);
        check_ok(ok, "inserted key 1");
        ok = sh_table_insert(table, "gpu", 2);
        check_ok(ok, "inserted key 2");
        ok = sh_table_insert(table, "largemem", 3);
        check_ok(ok, "inserted key 3");
        ok = sh_table_insert(table, "ibfdr", 4);
        check_ok(ok, "inserted key 4");

        // key collision should return false
        ok = sh_table_insert(table, "ibfdr", 5);
        check_ok(!ok, "can't insert duplicate key");

        // check that they hold the correct value
        uint32_t res = 0;
        ok = sh_table_get(table, "norm", &res);
        check_ok(ok && res == 1, "key 1 returned correct value");
        ok = sh_table_get(table, "gpu", &res);
        check_ok(ok && res == 2, "key 2 returned correct value");
        ok = sh_table_get(table, "largemem", &res);
        check_ok(ok && res == 3, "key 3 returned correct value");
        ok = sh_table_get(table, "ibfdr", &res);
        check_ok(ok && res == 4, "key 4 returned correct value");

        // get non existent key
        ok = sh_table_get(table, "nosuchkey", &res);
        check_ok(((!ok) && (res == 0)), "non-existent key returns false and value 0");

        sh_table_free(&table);
    }
}
#endif
