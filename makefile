SHELL := /bin/bash

HTSLIB = /usr/local/apps/samtools/1.3

.PHONY: help

PROGS := csr
OBJS  := csr.o \
		qual_scale.o \
		fqinfo.o \
		readlen.o \
		common.o \
		binbam.o \
		argtable3.o
CFLAGS := --std=gnu99 -Wall -Wextra -pedantic -Wshadow -Wpointer-arith
CFLAGS += -Wcast-qual -Wstrict-prototypes
CFLAGS += -O2 -I $(HTSLIB)/include
LIBS    := $(HTSLIB)/lib/libhts.a -lz -lpthread -lm -ldl

all: $(PROGS)

csr: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

readlen.o: readlen.c common.h debug.h
qual_scale: qual_scale.c common.h debug.h
binbam.o: binbam.c common.h debug.h argtable3.h
